<?php

namespace App\Twig;

use App\Service\TaxesService;
use Twig\Extension\AbstractExtension;
use Twig\TwigFunction;

class TaxesExtension extends AbstractExtension
{
    /**
     * @var TaxesService
     */
    private TaxesService $taxesService;

    public function __construct(TaxesService $taxesService)
    {
        $this->taxesService = $taxesService;
    }

    public function getFunctions()
    {
        return [
            new TwigFunction('getPriceWithVat', [$this, 'getPriceWithVat']),
        ];
    }

    public function getPriceWithVat($price): string
    {
        return $this->taxesService->getPriceWithTaxes($price);
    }
}
