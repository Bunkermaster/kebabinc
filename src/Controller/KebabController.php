<?php

namespace App\Controller;

use App\Entity\Kebab;
use App\Form\KebabType;
use App\Repository\KebabRepository;
use App\Service\TaxesService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/")
 */
class KebabController extends AbstractController
{
    /**
     * @Route("/", name="kebab_index", methods={"GET"})
     */
    public function index(KebabRepository $kebabRepository): Response
    {
        return $this->render('kebab/index.html.twig', [
            'kebabs' => $kebabRepository->findAll(),
        ]);
    }

    /**
     * Filters all kebabs with price superior to {price}.
     *
     * @Route("/filter/{price}", methods={"GET"})
     *
     * @param int             $price
     * @param KebabRepository $kebabRepository
     *
     * @return Response
     */
    public function filterKebabs($price, KebabRepository $kebabRepository): Response
    {
        if (!is_numeric($price)) {
            return new Response("Dommage pour le type", 400);
        }

        return $this->render('kebab/index.html.twig', [
            'kebabs' => $kebabRepository->getPriceSup($price),
        ]);
    }

    /**
     * @Route("/new", name="kebab_new", methods={"GET","POST"})
     */
    public function new(Request $request): Response
    {
        $kebab = new Kebab();
        $form = $this->createForm(KebabType::class, $kebab);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($kebab);
            $entityManager->flush();

            return $this->redirectToRoute('kebab_index');
        }

        return $this->render('kebab/new.html.twig', [
            'kebab' => $kebab,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="kebab_show", methods={"GET"})
     */
    public function show(Kebab $kebab, TaxesService $taxesService): Response
    {
        return $this->render('kebab/show.html.twig', [
            'kebab' => $kebab,
            'priceWithVat' => $taxesService->getPriceWithTaxes($kebab->getPrice()),
        ]);
    }

    /**
     * @Route("/{id}/edit", name="kebab_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, Kebab $kebab): Response
    {
        $form = $this->createForm(KebabType::class, $kebab);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('kebab_index');
        }

        return $this->render('kebab/edit.html.twig', [
            'kebab' => $kebab,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="kebab_delete", methods={"DELETE"})
     */
    public function delete(Request $request, Kebab $kebab): Response
    {
        if ($this->isCsrfTokenValid('delete'.$kebab->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($kebab);
            $entityManager->flush();
        }

        return $this->redirectToRoute('kebab_index');
    }
}
