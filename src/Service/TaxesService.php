<?php

namespace App\Service;

class TaxesService
{
    private const VAT_RATE = 20;

    public function getPriceWithTaxes(float $priceWithoutVat): float
    {
        return $priceWithoutVat * (1 + static::VAT_RATE / 100);
    }
}
