<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200421133533 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE kebab_ingredient DROP FOREIGN KEY FK_E744952BD86CD3C0');
        $this->addSql('ALTER TABLE kebab CHANGE id id INT UNSIGNED AUTO_INCREMENT NOT NULL');
        $this->addSql('ALTER TABLE kebab_ingredient CHANGE kebab_id kebab_id INT UNSIGNED NOT NULL');
        $this->addSql('ALTER TABLE kebab_ingredient ADD CONSTRAINT FK_E744952BD86CD3C0 FOREIGN KEY (kebab_id) REFERENCES kebab (id) ON DELETE CASCADE');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE kebab_ingredient DROP FOREIGN KEY FK_E744952BD86CD3C0');
        $this->addSql('ALTER TABLE kebab CHANGE id id INT AUTO_INCREMENT NOT NULL');
        $this->addSql('ALTER TABLE kebab_ingredient CHANGE kebab_id kebab_id INT NOT NULL');
        $this->addSql('ALTER TABLE kebab_ingredient ADD CONSTRAINT FK_E744952BD86CD3C0 FOREIGN KEY (kebab_id) REFERENCES kebab (id) ON DELETE CASCADE');
    }
}
